//
//  ViewController.swift
//  Reverse Geocoding
//
//  Created by Arash Sadeghieh E on 7/09/2015.
//  Copyright (c) 2015 Arash Sadeghieh Eshtehadi. All rights reserved.
//

import UIKit
import CoreLocation
import AddressBook
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var addressLabel: UITextField!
    @IBOutlet weak var cityLabel: UITextField!
    @IBOutlet weak var stateLabel: UITextField!
    @IBOutlet weak var zipLabel: UITextField!
    @IBOutlet weak var showMapButton: UIButton!
    
    
    var coords: CLLocationCoordinate2D?
    var latt:CLLocationDegrees?
    var lonn:CLLocationDegrees?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        showMapButton.layer.cornerRadius = 4
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "mapSegue" {
            let mapvc = segue.destinationViewController as! MapViewController
            
//            mapvc.lat = latt
//            mapvc.lon = lonn
            mapvc.coords = coords
        }
    }

    @IBAction func getDirections(sender: UIButton) {
        let geoCoder = CLGeocoder()
        
        let addressString = "\(addressLabel.text) \(cityLabel.text) \(stateLabel.text) \(zipLabel.text)"
        if (addressLabel.text.isEmpty || cityLabel.text.isEmpty || stateLabel.text.isEmpty || zipLabel.text.isEmpty) {
            addressAlert()
        } else {
            geoCoder.geocodeAddressString(addressString, completionHandler:
                {(placemarks: [AnyObject]!, error: NSError!) in
                    
                    if error != nil {
                        println("Geocode failed with error: \(error.localizedDescription)")
                    } else if placemarks.count > 0 {
                        let placemark = placemarks[0] as! CLPlacemark
                        let location = placemark.location
                        self.coords = location.coordinate
                        
//                        self.latt = self.coords?.latitude
//                        self.lonn = self.coords?.longitude
                        
                        self.showMap()
                        
                    }
            })
        }
        
    }
    
    func showMap() {
        performSegueWithIdentifier("mapSegue", sender: nil)
    }
    
    func addressAlert() {
        var alert = UIAlertController(title: "Blank Address", message: "Please enter the address!", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

}

