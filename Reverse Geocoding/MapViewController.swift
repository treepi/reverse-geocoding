//
//  MapViewController.swift
//  Reverse Geocoding
//
//  Created by Arash Sadeghieh E on 7/09/2015.
//  Copyright (c) 2015 Arash Sadeghieh Eshtehadi. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate {
    
//    var lat: CLLocationDegrees?
//    var lon: CLLocationDegrees?
    var coords: CLLocationCoordinate2D?

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var location = CLLocationCoordinate2DMake(coords!.latitude, coords!.longitude)
        
        //Draw a circle of 100m radius around user location
        self.mapView.delegate = self
        var newCircle = MKCircle(centerCoordinate: location, radius: 100 as CLLocationDistance)
        
        var span = MKCoordinateSpanMake(0.02, 0.02)
        var region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
        
        var annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Bashgah"
        annotation.subtitle = "My Personal Gym Location"
        mapView.addAnnotation(annotation)
        
        mapView.addOverlay(newCircle)
        
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKCircle {
            var circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor(red: 0, green: 100, blue: 255, alpha: 1.0)
            circle.fillColor = UIColor(red: 0, green: 100, blue: 255, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
